﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SocialMedia.Models;
using SocialMedia.Providers;

namespace SocialMedia.Repositories
{
    /// <summary>
    /// Interface that is used together with the SimpleMembershipProvider.
    /// </summary>
    public interface IUserRepository
    {
        User GetByUserName(string username);
    }

    public class UserRepository : IUserRepository
    {
        // The context
        protected SocialMediaContext db;

        /// <summary>
        /// Initialize a new instance of the 
        /// UserRepository with the provided context.
        /// </summary>
        /// <param name="context">The context.</param>
        public UserRepository(SocialMediaContext context)
        {
            db = context;
        }

        /// <summary>
        /// Add a user to the data storage.
        /// </summary>
        /// <param name="user">The user to add.</param>
        public void Add(User user)
        {
            // Encrypt password before saving to database
            user.Password = SimpleMembershipProvider.EncryptPassword(user.Password);

            db.Users.Add(user);

            db.SaveChanges();
        }

        /// <summary>
        /// Retrive a user by its username.
        /// </summary>
        /// <param name="username">The username.</param>
        public User GetByUserName(string username)
        {
            var user = db.Users.FirstOrDefault(u => u.Username.ToLower() == username.ToLower());

            return user;
        }

        public IEnumerable<User> All() {
            return db.Users.ToList();
        }

        /// <summary>
        /// Retrive a user by its ID.
        /// </summary>
        /// <param name="username">The id.</param>
        public User GetUserById(int userId)
        {
            return db.Users.FirstOrDefault(u => u.Id == userId);
        }

        public IEnumerable<User> SearchByUserName(string username)
        {
            return db.Users.Where(u => u.Username.Contains(username)).ToList();
        }
    }
}