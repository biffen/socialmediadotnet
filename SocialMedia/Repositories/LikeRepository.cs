﻿using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SocialMedia.Repositories
{
    public class LikeRepository
    {
        protected SocialMediaContext db;

        public LikeRepository(SocialMediaContext context)
        {
            db = context;
        }

        /// <summary>
        /// Adds a Like object to the database and saves changes.
        /// </summary>
        /// <param name="like">The Like object to add</param>
        public void Add(Like like)
        {
            db.Likes.Add(like);
            db.SaveChanges();
        }

        /// <summary>
        /// Removes a like from the database that matches the given user ID and post ID
        /// </summary>
        /// <param name="userId">ID of the user</param>
        /// <param name="postId">ID of the post</param>
        public void Remove(int userId, int postId)
        {
            db.Likes.Remove(Find(userId, postId));
            db.SaveChanges();
        }

        /// <summary>
        /// Returns a like that matches the given user ID and post ID
        /// </summary>
        /// <param name="userId">The ID of the user.</param>
        /// <param name="postId">The ID of the post.</param>
        /// <returns>The found Like or default value (null). </returns>
        public Like Find(int userId, int postId)
        {
            return db.Likes
                .Where(l => l.User.Id == userId && l.Post.Id == postId).FirstOrDefault();
        }

        /// <summary>
        /// Gets the number of likes a given post has.
        /// </summary>
        /// <param name="PostId">The ID of the post.</param>
        /// <returns>The number of likes the post has. </returns>
        public int GetNumberOfLikesByPost(int PostId)
        {
            return db.Likes.
                Where(l => l.Post.Id == PostId).Count();
        }

        /// <summary>
        /// Returns all Like objects that belongs to a certain post
        /// </summary>
        /// <param name="postId">The ID of the post</param>
        /// <returns>List of the like objects</returns>
        public List<Like> GetLikesByPost(int postId)
        {
            return db.Likes.Where(l => l.Post.Id == postId).ToList();
        }

        /// <summary>
        /// Checks whether the given user likes the given post.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="post">The post.</param>
        /// <returns>True if the user likes the post, otherwise false. </returns>
        public bool UserLikesPost(User user, Post post)
        {
            return db.Likes
                .Where(l => l.User.Id == user.Id && l.Post.Id == post.Id).Count() > 0;
        }

    }
}