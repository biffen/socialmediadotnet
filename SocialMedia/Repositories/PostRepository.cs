﻿using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SocialMedia.Repositories
{
    public class PostRepository
    {
        protected SocialMediaContext db;

        public PostRepository(SocialMediaContext context)
        {
            db = context;
        }

        /// <summary>
        /// Returns all posts with their User included and ordered by datetime descending. 
        /// </summary>
        /// <returns>List of all posts. </returns>
        public IEnumerable<Post> All()
        {
            return db.Posts.Include(p => p.User).OrderByDescending(p => p.Time).ToList();
        }

        /// <summary>
        /// Get all posts made by a certain user.
        /// </summary>
        /// <param name="userId">The id of the user.</param>
        /// <returns>The list of posts. </returns>
        public List<Post> GetPostsByUserId(int userId)
        {
            return db.Posts.Include(p => p.User).Where(p => p.User.Id == userId).OrderByDescending(p => p.Time).ToList();
        }

        /// <summary>
        /// Adds a post to the database. 
        /// </summary>
        /// <param name="post">The post to add. </param>
        public void Add(Post post)
        {
            db.Posts.Add(post);
            db.SaveChanges();
        }

        /// <summary>
        /// Get a post object based on its ID.
        /// </summary>
        /// <param name="postId">The ID of the post. </param>
        /// <returns>The post object or default value (null).</returns>
        public Post GetPostById(int postId)
        {
            var post = db.Posts.FirstOrDefault(p => p.Id == postId);

            return post;
        }

        /// <summary>
        /// Gets all posts that are made by users that the given user is following.
        /// </summary>
        /// <param name="user">The current user.</param>
        /// <returns>A list with the posts. </returns>
        public List<Post> GetFollowedPosts(User user)
        {
            List<Follow> follows = db.Follows.
                Where(f => f.UserFollows.Id == user.Id).
                Include(f => f.UserFollowed).
                ToList();
            
            List<int> followsIds = new List<int>();
            foreach(Follow follow in follows)
            {
                followsIds.Add(follow.UserFollowed.Id);
            }

            List<Post> followedPosts = new List<Post>();
            foreach (Post post in All())
            {
                if (followsIds.IndexOf(post.User.Id) != -1 || post.User.Id == user.Id)
                    followedPosts.Add(post);
            }

            return followedPosts;
        }
    }
}