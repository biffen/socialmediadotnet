﻿using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace SocialMedia.Repositories
{
    public class ReplyRepository
    {
        protected SocialMediaContext db;

        public ReplyRepository(SocialMediaContext context)
        {
            db = context;
        }

        /// <summary>
        /// Returns all replies with the user object included.
        /// </summary>
        /// <returns>List with all posts. </returns>
        public IEnumerable<Reply> All()
        {
            return db.Replies.Include(r => r.User).ToList();
        }

        /// <summary>
        /// Adds a Reply object to the database. 
        /// </summary>
        /// <param name="reply">The reply object. </param>
        public void Add(Reply reply)
        {
            db.Replies.Add(reply);
            db.SaveChanges();
        }

        /// <summary>
        /// Returns all replies that belong to a given post.
        /// </summary>
        /// <param name="postId">The id of the post.</param>
        /// <returns>A list with all replies. </returns>
        public List<Reply> GetRepliesByPost(int postId)
        {
            var replies = db.Replies.Where(r => r.Post.Id == postId).Include(r => r.User);

            return replies.ToList();
        }
    }
}