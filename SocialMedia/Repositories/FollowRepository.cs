﻿using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace SocialMedia.Repositories
{
    public class FollowRepository
    {
        protected SocialMediaContext db;

        public FollowRepository(SocialMediaContext context)
        {
            db = context;
        }

        /// <summary>
        /// Adds the given Follow object to the database. 
        /// </summary>
        /// <param name="follow">The Follow object to add. </param>
        public void Add(Follow follow)
        {
            db.Follows.Add(follow);
            db.SaveChanges();
        }

        /// <summary>
        /// Removes a Follow object from the database.
        /// </summary>
        /// <param name="userFollowsId">The ID of the user that is following (current user usually).</param>
        /// <param name="userFollowedId">The ID of the user that is followed. </param>
        public void Remove(int userFollowsId, int userFollowedId)
        {
            Follow selectedFollow = Find(userFollowsId, userFollowedId);

            if (selectedFollow != null)
            {
                db.Follows.Remove(selectedFollow);
                db.SaveChanges();
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Could not remove follow, did not exist.");
            }
        }

        /// <summary>
        /// Finds and returns a Follow object in the database based on user IDs. 
        /// </summary>
        /// <param name="userFollowsId">The user ID of the user that is following someone.</param>
        /// <param name="userFollowedId">The user ID of the user that is being followed.</param>
        /// <returns>The found Follow object or default value (null).</returns>
        public Follow Find(int userFollowsId, int userFollowedId)
        {
            return db.Follows.
                Where(f => f.UserFollows.Id == userFollowsId && f.UserFollowed.Id == userFollowedId).
                FirstOrDefault();
        }

        /// <summary>
        /// Get all the Follow objects based on which user is following. 
        /// </summary>
        /// <param name="userId">The ID of the user following. </param>
        /// <returns>The list of follows.</returns>
        public IEnumerable<Follow> GetFollowsByUserFollows(int userId)
        {
            return db.Follows.
                Where(f => f.UserFollows.Id == userId).
                Include(f => f.UserFollowed).
                Include(f => f.UserFollows).
                ToList();
        }

        /// <summary>
        /// Get all the Follow objects based on which user is being followed. 
        /// </summary>
        /// <param name="userId">The ID of the user being followed. </param>
        /// <returns>The list of follows.</returns>
        public IEnumerable<Follow> GetFollowsByUserFollowed(int userId)
        {
            return db.Follows.
                Where(f => f.UserFollowed.Id == userId).
                Include(f => f.UserFollowed).
                Include(f => f.UserFollows).
                ToList();
        }

        /// <summary>
        /// Checks whether a certain user is following another. 
        /// </summary>
        /// <param name="userFollowsId">The id of the user following. </param>
        /// <param name="userFollowedId">The id of the user being followed..</param>
        /// <returns>True if the user is following, otherwise false.</returns>
        public bool IsFollowing(int userFollowsId, int userFollowedId)
        {
            return db.Follows.
                Where(f => f.UserFollows.Id == userFollowsId &&
                           f.UserFollowed.Id == userFollowedId).
                Count() > 0;
        }

    }
}