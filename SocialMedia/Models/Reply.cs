﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SocialMedia.Models
{
    [Table("Replies")]
    public class Reply
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public Post Post { get; set; }

        [Required]
        public User User { get; set; }

        [Required]
        public DateTime Time { get; set; }

        [Required]
        public string Content { get; set; }
    }
}