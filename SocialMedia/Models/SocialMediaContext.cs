﻿using System;
using System.Data.Entity;
using MySql.Data.Entity;
using SocialMedia.Providers;

namespace SocialMedia.Models
{
    /// <summary>
    /// Derived context.
    /// </summary>
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class SocialMediaContext : DbContext
    {
        public SocialMediaContext()
            : base("SocialMediaMySQLConnection")
        {
            // Database strategy
            Database.SetInitializer<SocialMediaContext>(new SocialMediaDbInit());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Reply> Replies { get; set; }
        public DbSet<Follow> Follows { get; set; }
        public DbSet<Like> Likes { get; set;  }


        // If you want to try or need to (some use cases) use fluent API this is the place!
        // Reference: http://blogs.msdn.com/b/adonet/archive/2010/12/14/ef-feature-ctp5-fluent-api-samples.aspx
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Reply>()
                .HasRequired(r => r.User)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Post>()
                .HasRequired(p => p.User)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Follow>()
                .HasRequired(f => f.UserFollowed)
                .WithMany()
                .WillCascadeOnDelete(false);
            
            modelBuilder.Entity<Follow>()
                .HasRequired(f => f.UserFollows)
                .WithMany()
                .WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }
    }

    /// <summary>
    /// Derived database strategy
    /// </summary>
    class SocialMediaDbInit : DropCreateDatabaseAlways<SocialMediaContext>
    {
        protected override void Seed(SocialMediaContext context)
        {
            base.Seed(context);

            // Seed your data here!


            //
            // Users
            //
            User userOne = new User()
            {
                Email = "adde.addthorn@gmail.com",
                Username = "thebiffman",
                Password = SimpleMembershipProvider.EncryptPassword("hej123")
            };

            User userTwo = new User()
            {
                Email = "andreas@biffnet.se",
                Username = "adde",
                Password = SimpleMembershipProvider.EncryptPassword("hej123")
            };

            User userThree = new User()
            {
                Email = "d.m.andersson@gmail.com",
                Username = "daniel",
                Password = SimpleMembershipProvider.EncryptPassword("hej123")
            };

            User userFour = new User()
            {
                Email = "kalle.anka@ankeborg.se",
                Username = "kalle",
                Password = SimpleMembershipProvider.EncryptPassword("hej123")
            };


            //
            // Posts
            //
            Post postOne = new Post()
            {
                Content = "Barnaby The Bear's my name, never call me Jack or James, I will sing my way to fame, Barnaby the Bear's my name. Birds taught me to sing, when they took me to their king, first I had to fly, in the sky so high so high, so high so high so high, so - if you want to sing this way, think of what you'd like to say, add a tune and you will see, just how easy it can be.",
                Time = DateTime.Now,
                User = userOne
            };

            Post postTwo = new Post()
            {
                Content = "Mutley, you snickering, floppy eared hound. When courage is needed, you're never around. Those medals you wear on your moth-eaten chest should be there for bungling at which you are best.",
                Time = DateTime.Now,
                User = userOne
            };

            Post postThree = new Post()
            {
                Content = "80 days around the world, we'll find a pot of gold just sitting where the rainbow's ending. Time - we'll fight against the time, and we'll fly on the white wings of the wind.",
                Time = DateTime.Now,
                User = userTwo
            };

            Post postFour = new Post()
            {
                Content = "There's a voice that keeps on calling me. Down the road, that's where I'll always be. Every stop I make, I make a new friend. Can't stay for long, just turn around and I'm gone again. Maybe tomorrow, I'll want to settle down, Until tomorrow, I'll just keep moving on.",
                Time = DateTime.Now,
                User = userThree
            };


            //
            // Posts
            //
            Reply replyOne = new Reply()
            {
                Content = "That bear is cool, eh!?",
                Post = postOne,
                Time = DateTime.Now,
                User = userFour
            };

            Reply replyTwo = new Reply()
            {
                Content = "Indeed he is!",
                Post = postOne,
                Time = DateTime.Now,
                User = userOne
            };

            Reply replyThree = new Reply()
            {
                Content = "80 days is quite a lot! 80 days is quite a lot! 80 days is quite a lot! 80 days is quite a lot! 80 days is quite a lot! 80 days is quite a lot! ",
                Post = postThree,
                Time = DateTime.Now,
                User = userFour
            };

            Reply replyFour = new Reply()
            {
                Content = "You go down that road! wooo!",
                Post = postFour,
                Time = DateTime.Now,
                User = userTwo
            };

            Follow followOne = new Follow()
            {
                UserFollows = userOne,
                UserFollowed = userTwo
            };

            Follow followTwo = new Follow()
            {
                UserFollows = userOne,
                UserFollowed = userThree
            };


            context.Users.Add(userOne);
            context.Users.Add(userTwo);
            context.Users.Add(userThree);
            context.Users.Add(userFour);

            context.Posts.Add(postOne);
            context.Posts.Add(postTwo);
            context.Posts.Add(postThree);
            context.Posts.Add(postFour);

            context.Replies.Add(replyOne);
            context.Replies.Add(replyTwo);
            context.Replies.Add(replyThree);
            context.Replies.Add(replyFour);

            context.Follows.Add(followOne);
            context.Follows.Add(followTwo);

            context.SaveChanges();

        }
    }
}