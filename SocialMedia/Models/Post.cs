﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SocialMedia.Models
{
    [Table("Posts")]
    public class Post
    {
        public Post()
        {
            Replies = new List<Reply>();
            Likes = new List<Like>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public User User { get; set; }

        [Required]
        public DateTime Time { get; set; }

        [Required]
        public string Content { get; set; }

        [NotMapped]
        public List<Reply> Replies { get; set; }

        [NotMapped]
        public List<Like> Likes { get; set; }
        
        [NotMapped]
        public int NumOfLikes
        {
            get { return Likes.Count; }
        }

        public bool UserHasLiked(int userId)
        {
            return Likes.Any(l => l.User.Id == userId);
        }
    }
}
