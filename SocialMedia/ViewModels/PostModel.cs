﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SocialMedia.ViewModels
{
    public class PostModel
    {
        [Required]
        [Display(Name = "Post")]
        public string PostContent { get; set; }

        [Required]
        public int UserId { get; set; }

    }
}