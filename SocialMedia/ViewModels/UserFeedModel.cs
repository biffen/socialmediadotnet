﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SocialMedia.Models;

namespace SocialMedia.ViewModels
{
    public class UserFeedModel
    {
        public User User { get; set; }
        public List<Post> PostList { get; set; }

        public UserFeedModel(User user, List<Post> postList)
        {
            User = user;
            PostList = postList;
        }
    }
}