﻿using SocialMedia.Models;
using SocialMedia.Repositories;
using SocialMedia.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SocialMedia.Controllers
{
    public class PostController : Controller
    {
        //
        // GET: /Post/
        [Authorize]
        public ActionResult Index()
        {

            // Get data

            SocialMedia.Repositories.UserRepository userRepository = SocialMedia.Repositories.RepositoryFactory.GetUserRepository();
            SocialMedia.Repositories.ReplyRepository replyRepository = SocialMedia.Repositories.RepositoryFactory.GetReplyRepository();
            SocialMedia.Repositories.LikeRepository likeRepository = SocialMedia.Repositories.RepositoryFactory.GetLikeRepository();
            SocialMedia.Repositories.PostRepository postRepository = SocialMedia.Repositories.RepositoryFactory.GetPostRepository();

            User currentUser = userRepository.GetByUserName(User.Identity.Name);
            
            //var allPosts = _postRepository.All();
            var allPosts = postRepository.GetFollowedPosts(currentUser);

            foreach (Post post in allPosts)
            {
                // Retrieve all replies for this post
                List<Reply> replies = replyRepository.GetRepliesByPost(post.Id);

                // Retrieve the number of likes for this post
                List<Like> likes = likeRepository.GetLikesByPost(post.Id);

                // Add the replies and the number of likes to the post object
                post.Replies.AddRange(replies);
                post.Likes.AddRange(likes);
            }

            // Fill the FeedModel with all data
            FeedModel feedModel = new FeedModel
            {
                AllPosts = allPosts,
                CurrentUser = currentUser
            };
   

            // Return view with ViewModel as parameter

            return View(feedModel);
        }

        //
        // GET: /Post/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Post/Add
        public ActionResult Add()
        {
            return View();
        }

        //
        // POST: /Post/Add
        [HttpPost]
        public ActionResult Add(int UserId, string PostContent)
        {
            try
            {
                UserRepository userRepository = RepositoryFactory.GetUserRepository();
                PostRepository postRepository = RepositoryFactory.GetPostRepository();

                User currentUser = userRepository.GetUserById(UserId);

                Post newPost = new Post()
                {
                    Content = PostContent,
                    User = currentUser,
                    Time = DateTime.Now
                };

                postRepository.Add(newPost);

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Post/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Post/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Post/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Post/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
