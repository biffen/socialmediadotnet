﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SocialMedia.Repositories;
using SocialMedia.Models;

namespace SocialMedia.Controllers
{
    public class FollowController : Controller
    {
        //
        // GET: /Follow/
        public ActionResult Index()
        {
            return View();
        }

        //
        // POST: /Follow/Add
        [HttpPost]
        public ActionResult Add(int FollowedUser, int FollowsUser, string ReturnUrl)
        {
            try
            {
                FollowRepository followRepository = RepositoryFactory.GetFollowRepository();
                UserRepository userRepository = RepositoryFactory.GetUserRepository();

                User userFollows = userRepository.GetUserById(FollowsUser);
                User userFollowed = userRepository.GetUserById(FollowedUser);

                Follow newFollow = new Follow()
                {
                    UserFollowed = userFollowed,
                    UserFollows = userFollows
                };

                followRepository.Add(newFollow);

                return Redirect(ReturnUrl);
            }
            catch
            {
                return Redirect(ReturnUrl);
            }
        }

        //
        // POST: /Follow/Remove
        [HttpPost]
        public ActionResult Remove(int FollowedUser, int FollowsUser, string ReturnUrl)
        {
            try
            {
                FollowRepository followRepository = RepositoryFactory.GetFollowRepository();
                UserRepository userRepository = RepositoryFactory.GetUserRepository();

                User userFollows = userRepository.GetUserById(FollowsUser);
                User userFollowed = userRepository.GetUserById(FollowedUser);

                followRepository.Remove(userFollows.Id, userFollowed.Id);

                return Redirect(ReturnUrl);
            }
            catch
            {
                return Redirect(ReturnUrl);
            }
        }

    }
}
