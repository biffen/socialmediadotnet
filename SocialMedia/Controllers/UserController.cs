﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SocialMedia.Models;
using SocialMedia.ViewModels;
using SocialMedia.Repositories;

namespace SocialMedia.Controllers
{
    public class UserController : Controller
    {
        UserRepository _userRepository = RepositoryFactory.GetUserRepository();

        /// <summary>
        /// GET: /User/
        /// </summary>
        /// <returns>The ActionResult.</returns>
        [Authorize]
        public ActionResult Index()
        {
            var user = this.User;

            var allUser = _userRepository.All();

            return View(allUser);
        }

        /// <summary>
        /// GET: /User/Feed/1
        /// </summary>
        /// <param name="id">The id of the user</param>
        /// <returns>The ActionResult. </returns>
        [Authorize]
        public ActionResult Feed(int id)
        {
            UserRepository userRepo = RepositoryFactory.GetUserRepository();
            PostRepository postRepo = RepositoryFactory.GetPostRepository();

            User currentUser = userRepo.GetUserById(id);
            List<Post> postsByUser = postRepo.GetPostsByUserId(id);

            UserFeedModel feedModel = new UserFeedModel(currentUser, postsByUser);

            return View(feedModel);
        }

        /// <summary>
        /// POST: /User/Search. 
        /// </summary>
        /// <param name="UserName">The complete or partial username to search for.</param>
        /// <returns>The ActionResult.</returns>
        [HttpPost]
        [Authorize]
        public ActionResult Search(string UserName)
        {
            System.Diagnostics.Debug.WriteLine("UserName:" + UserName);

            var foundUsers = _userRepository.SearchByUserName(UserName);
            
            System.Diagnostics.Debug.WriteLine("foundUsers size:" + foundUsers.Count().ToString());

            return View("Index", foundUsers);
        }

    }
}
