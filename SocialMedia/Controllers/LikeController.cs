﻿using SocialMedia.Repositories;
using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SocialMedia.Controllers
{
    public class LikeController : Controller
    {
        //
        // GET: /Like/
        public ActionResult Index()
        {
            return View();
        }

        //
        // POST: /Like/Add/Id
        [Authorize]
        [HttpPost]
        public ActionResult Add(int UserId, int PostId, string ReturnUrl)
        {
            try
            {
                UserRepository userRepository = RepositoryFactory.GetUserRepository();
                PostRepository postRepository = RepositoryFactory.GetPostRepository();
                LikeRepository likeRepository = RepositoryFactory.GetLikeRepository();

                User currentUser = userRepository.GetUserById(UserId);
                Post currentPost = postRepository.GetPostById(PostId);

                Like newLike = new Like()
                {
                    User = currentUser,
                    Post = currentPost
                };

                likeRepository.Add(newLike);

                return Redirect(ReturnUrl);
            }
            catch
            {
                return Redirect(ReturnUrl);
            }
        }

        //
        // POST: /Like/Remove/Id
        [Authorize]
        [HttpPost]
        public ActionResult Remove(int UserId, int PostId, string ReturnUrl)
        {
            try
            {
                LikeRepository likeRepository = RepositoryFactory.GetLikeRepository();

                likeRepository.Remove(UserId, PostId);

                return Redirect(ReturnUrl);
            }
            catch
            {
                return Redirect(ReturnUrl);
            }
        }

    }
}
