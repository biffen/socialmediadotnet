﻿using SocialMedia.Repositories;
using SocialMedia.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SocialMedia.Models;

namespace SocialMedia.Controllers
{
    public class ReplyController : Controller
    {

        //
        // POST: /Post/Add
        [HttpPost]
        public ActionResult Add(string ReplyContent, int UserId, int PostId, string returnUrl)
        {
            try
            {
                UserRepository userRepository = RepositoryFactory.GetUserRepository();
                PostRepository postRepository = RepositoryFactory.GetPostRepository();
                ReplyRepository replyRepository = RepositoryFactory.GetReplyRepository();

                User currentUser = userRepository.GetUserById(UserId);
                Post currentPost = postRepository.GetPostById(PostId);

                Reply newReply = new Reply()
                {
                    Content = ReplyContent,
                    Post = currentPost,
                    User = currentUser,
                    Time = DateTime.Now
                };

                replyRepository.Add(newReply);

                if (returnUrl != null)
                    return Redirect(returnUrl);

                return RedirectToAction("Index", "Post");
            }
            catch
            {
                return RedirectToAction("Index", "Post");
            }
        }

    }
}
